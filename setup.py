from setuptools import setup

VERSION = 0.1

requirements = [
    'matplotlib>=1.5, <2.0a0',
    'numpy>=1.10.1, <2.0a0',
    'pandas>=0.16.2, <1.0a0',
    'scikit-learn>=0.16.1, <1.0a0',
    'DateTime>=4.0.1, <5.0a0',
    'scipy<0.15.1, <1.0a0'
]


def main():
    setup(
        name='frigg',
        version=VERSION,
        zip_safe=False,
        install_requires=requirements
    )


if __name__ == '__main__':
    main()
