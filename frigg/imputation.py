import numpy as np
from copy import deepcopy
from collections import defaultdict


def get_mean_by_class(num_var, class_var, vals_to_exclude=None):
    """
    calculate the means of a numerical variable, grouped using a class variable
    :param num_var: numerical value
    :param class_var:
    :param vals_to_exclude:
    :return:
    """
    if vals_to_exclude is None:
        vals_to_exclude = []

    # create a dictionary relating each class of class_var to the numerical values from the same observations
    class_vals = [(cl, val) for cl, val in zip(class_var, num_var) if val not in vals_to_exclude]
    class_val_dict = defaultdict(list)
    for k, v in class_vals:
        class_val_dict[k].append(v)

    class_means = {}

    for cl, vals in class_val_dict.iteritems():
        total = sum(vals)
        n_obs = len(vals)
        mean = float(total)/n_obs
        mean = np.mean(vals)
        class_means[cl] = mean

    return class_means


def impute_num_var_with_mean_by_class(num_var, class_var, vals_to_impute=None):
    if vals_to_impute is None:
        vals_to_impute = []
    means_by_class = get_mean_by_class(num_var, class_var, vals_to_exclude=vals_to_impute)

    num_var_new = deepcopy(num_var)
    num_var_new.name = "imp_" + num_var.name
    imputed = []
    for i, val in enumerate(num_var_new):
        if val in vals_to_impute:
            num_var_new[i] = means_by_class[class_var[i]]
            imputed.append(True)
        else:
            imputed.append(False)
    return num_var_new, imputed


def impute_num_var_with_mean(col, vals_to_replace=None):
    if vals_to_replace is None:
        vals_to_replace = []

    values = [x for x in col if x not in vals_to_replace]
    mean_val = np.mean(values)
    new_col = deepcopy(col)
    new_col.name = "imp_" + col.name
    imputed = []
    for i, item in enumerate(new_col):
        if item in vals_to_replace:
            new_col[i] = mean_val
            imputed.append(True)
        else:
            imputed.append(False)
    return new_col, imputed


def impute_and_replace_cols(df, cols_to_impute, class_var=None, vals_to_replace=None):
    if vals_to_replace is None:
        vals_to_replace = []

    if class_var:
        class_col = df[class_var]

    for col_name in cols_to_impute:
        col = df[col_name]

        if not class_var:
            new_col, imputed = impute_num_var_with_mean(col, vals_to_replace)

        else:
            new_col, imputed = impute_num_var_with_mean_by_class(col, class_col, vals_to_replace)
        df["imp_" + col_name] = new_col
    df = df.drop(cols_to_impute, axis=1)
    return df

# TODO: imputation using models
