from exploratory import get_numeric_cols
import pandas as pd
import numpy as np
from sklearn.preprocessing import scale
from scipy.stats import boxcox
from DateTime import DateTime
from copy import deepcopy
import exploratory as explore


def box_cox_num_vars(df, exclude_list=None):
    if exclude_list is None:
        exclude_list = []

    for col_name in df.columns:
        if col_name in exclude_list:
            continue
        vals = df[col_name]
        if vals.dtype.name is 'int64':
            vals = vals.astype(float)
        if vals.dtype.name in ['float64']:
            min_val = min(vals)
            if min_val <= 0:
                vals += -1 * min_val + 1
            df[col_name], _ = boxcox(vals)
    return df


def standardise_num_vars(df, exclude_list=None):
    if exclude_list is None:
        exclude_list = []

    for column in df.columns:
        if column in exclude_list:
            continue
        col = df[column]
        if col.dtype.name == 'int64':
            col = col.astype(float)
        if col.dtype.name == "float64":
            df[column] = scale(col)
    return df


def impute_num_vars():
    # TODO

    print 'do stuff'


def dates_to_ints(dates, ref_date):
    """
    convert a sequence of dates into integers representing days in relation to a reference date
    :param dates: a sequence of datetimes
    :param ref_date: a reference datetime
    :return: a sequence of integers corresponding to days from reference date
    """
    days = [(ref_date - date).days for date in dates]
    return days


def datestrings_to_ints(datestrings, date_format, ref_date=None):
    """
    convert datestrings to integers representing days in relation to a reference date
    :param datestrings: a pandas sequence of datestrings
    :param date_format: the datetime format of the datestrings
    :param ref_date:
    :return: a sequence of integers
    """
    dates = [DateTime.strptime(datestring, date_format) for datestring in datestrings]
    sorted_unique_dates = sorted(set(dates))
    if not ref_date:
        ref_date = sorted_unique_dates[-1] + (sorted_unique_dates[-1] - sorted_unique_dates[-2])
    days = dates_to_ints(dates, ref_date)
    return days


def impute_class_var():
    # TODO

    print 'do stuff'


def class_variable_mapping(col, n=None):
    """
    create a mapping from each value of a class variable to a corresponding integer in order of frequency
    :param col: a sequence of values representing a class variable
    :param n: n+1 is the max # of classes in the new mapping; the n most common classes and the rest as an 'other' class
    :return: a dictionary mapping each value of the original sequence to an integer [0, n]
    """
    class_counts = explore.class_variable_counts(col)
    sorted_classes = sorted(class_counts.iteritems(), key=lambda (value, count): count, reverse=True)
    num_classes = len(sorted_classes)

    if n is None:
        n = num_classes

    mapping = {}

    if n < num_classes:
        for i in xrange(n):
            mapping[sorted_classes[i][0]] = i
        for val in sorted_classes[n:]:
            mapping[val[0]] = n
    else:
        for i, val in enumerate(sorted_classes):
            mapping[val[0]] = i

    return mapping


def encode_class_variable(col, mapping):
    """
    encode a class variable using a binary matrix
    :param col: a sequence of strings representing a class variable
    :param mapping: a dictionary mapping each possible value to an integer
    :return: a binary matrix where each column represents a column and each row consists of all 0s and a single 1
    """
    col = list(col)
    no_classes = len(set(mapping.values()))

    encoding = np.zeros((len(col), no_classes), dtype=np.int)

    for i in xrange(len(col)):
        val = col[i]
        encoding[i, mapping[val]] = 1

    return encoding


def encode_dataframe(df, class_var_limit=10):
    encoded_array = None

    for column in df.columns:
        column_data = df[column]
        if column_data.dtype.name == 'object':
            mapping = class_variable_mapping(column_data, class_var_limit)
            encoded_col = encode_class_variable(column_data, mapping)
        else:
            encoded_col = column_data.values.reshape((-1, 1))

        if encoded_array is None:
            encoded_array = encoded_col
        else:
            encoded_array = np.concatenate((encoded_array, encoded_col), axis=1)

    return encoded_array


def preprocess(preprocess_function, training_data, test_data, **params):
    """
    combines training and test data, applies a specified function to them, then splits them up and returns them
    :param preprocess_function: a function to use for preprocessing
    :param training_data: numpy array or pandas dataframe
    :param test_data: numpy array or pandas dataframe
    :param params: preprocess_function parameters
    :return: preprocessed versions of training and test data -- their form depends on the preprocess_function
    """
    training_data_num_rows = training_data.shape[0]

    combined_data = pd.concat([training_data, test_data])

    preprocessed_data = preprocess_function(combined_data, **params)

    training_data_preproc = preprocessed_data[:training_data_num_rows, :]
    test_data_preproc = preprocessed_data[training_data_num_rows:, :]

    return training_data_preproc, test_data_preproc
